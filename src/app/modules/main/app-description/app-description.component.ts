import { Component, OnInit } from "@angular/core";
import { ApiService } from "src/app/shared/service/api.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-app-description",
  templateUrl: "./app-description.component.html",
  styleUrls: ["./app-description.component.css"]
})
export class AppDescriptionComponent implements OnInit {
  popularApps: any = [];
  newestApps: any = [];
  constructor(private apiService: ApiService, private route:Router) {}

  ngOnInit() {
    this.getPopularAppList();
    this.getNewestAppList();
  }

  getPopularAppList() {
    this.apiService.getPopularApps().subscribe((data: any) => {
      this.popularApps = data.list;
    });
  }

  getNewestAppList() {
    this.apiService.getNewestApps().subscribe((data: any) => {
      this.newestApps = data.list;
    });
  }

  appDetails(safeName: string) {
    this.route.navigate(["/app-details/" + safeName]);
  }
}
