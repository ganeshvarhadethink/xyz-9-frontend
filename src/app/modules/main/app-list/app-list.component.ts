import { Component, OnInit } from "@angular/core";
import { ApiService } from "src/app/shared/service/api.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-app-list",
  templateUrl: "./app-list.component.html",
  styleUrls: ["./app-list.component.css"]
})
export class AppListComponent implements OnInit {
  paramSearch: string;
  searchedApps: any = [];
  selectedName: string;

  constructor(
    private router: ActivatedRoute,
    private route: Router,
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.apiService.$searchQuerySubject.subscribe(res => {
      if (Object.keys(res).length === 0) {
        // this.pageReloaded('all')
      } else {
        this.searchResult(res.toString());
      }
    });

    this.apiService.$selectedOptionSubject.subscribe((res: any) => {
      if (Object.keys(res).length === 0) {
        this.pageReloaded("all");
      } else {
        this.selectedName = res.selectedName;
        this.searchedApps = res.list;
      }
    });
  }

  searchResult(searchTextaValue: string) {
    this.apiService.getSearchApp(searchTextaValue).subscribe((data: any) => {
      this.selectedName =
        data.count + " SEARCH RESULT FOR " + searchTextaValue.toUpperCase();
      this.searchedApps = data.list;
    });
  }

  pageReloaded(searchTextaValue: string) {
    this.apiService.getSearchApp(searchTextaValue).subscribe((data: any) => {
      this.selectedName = "All Apps";
      this.searchedApps = data.list;
    });
  }

  appDetails(safeName: string) {
    this.route.navigate(["/app-details/" + safeName]);
  }
}
