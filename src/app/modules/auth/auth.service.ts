import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { fromPromise } from "rxjs/observable/fromPromise";
import { map, tap, catchError } from "rxjs/operators";
import { of } from "rxjs/observable/of";
import { Auth } from "aws-amplify";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  public loggedIn: BehaviorSubject<boolean>;
  constructor(private router: Router) {
    {
      this.loggedIn = new BehaviorSubject<boolean>(false);
    }
  }

  public signIn(email, password): Observable<any> {
    return fromPromise(Auth.signIn(email, password)).pipe(
      tap(() => this.loggedIn.next(true))
    );
  }

  public signUp(user): Observable<any> {
    return fromPromise(Auth.signUp(user));
  }

  public isAuthenticated(): Observable<boolean> {
    return fromPromise(Auth.currentAuthenticatedUser()).pipe(
      map(result => {
        this.loggedIn.next(true);
        return true;
      }),
      catchError(error => {
        this.loggedIn.next(false);
        return of(false);
      })
    );
  }

  public signOut() {
    fromPromise(Auth.signOut()).subscribe(
      result => {
        this.loggedIn.next(false);
        this.router.navigate(["/login"]);
      },
      error => console.log(error)
    );
  }

  // public async updateProfile(email: string) {
  //   let user = await Auth.currentAuthenticatedUser();
  //   let result = await Auth.updateUserAttributes(user, {
  //     email: email
  //   });
  //   return result;
  // }
  public async updateProfile(email: string) {
    try{
    const user = await Auth.currentAuthenticatedUser();
    let result = await Auth.updateUserAttributes(user, {
      email: email
    });
    return result;
  }catch(e){
    return e.message;
  }
  }

  public getUserLoginInfo() {
    return fromPromise(Auth.currentUserInfo());
  }

  public getcurrentAuthenticatedUser() {
    return fromPromise(Auth.currentAuthenticatedUser());
  }

  // public changePassword(oldPassword: string, newPassword: string) {
  //   return fromPromise(
  //     Auth.currentAuthenticatedUser()
  //       .then(user => {
  //         return Auth.changePassword(user, oldPassword, newPassword);
  //       })
  //       .then(data => {
  //         // console.log("data", data);
  //         return data;
  //       })
  //       .catch(err => {
  //         // console.log("err", err);
  //         return err;
  //       })
  //   );
  // }
  public async changePassword(oldPassword: string, newPassword: string) {
    try {
      const currentUser = await Auth.currentAuthenticatedUser();
     let result = await Auth.changePassword(
        currentUser,
       oldPassword,
        newPassword
      );
       return result;
       
    } catch (e) {
      //alert(e.message);
      return e.message;
    }
  }

  public forgotPassword(email: string) {
    return fromPromise(
      Auth.forgotPassword(email)
        .then(data => console.log(data))
        .catch(err => console.log(err))
    );

    // Collect confirmation code and new password, then
    // Auth.forgotPasswordSubmit(username, code, new_password)
    //     .then(data => console.log(data))
    //     .catch(err => console.log(err));
  }
}
