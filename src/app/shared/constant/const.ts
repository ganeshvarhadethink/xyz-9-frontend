export const ConstantClass = {
    'FEATURED_APP_QUERY_URL' : '{"status.value":"approved","attributes.featured":"yes"}',
    "FEATURED_APP_SORT_URL" : '{"randomize":1}',
    "POPULAR_APPS_QUERY_URL":'{"status.value":"approved"}',
    "POPULAR_APPS_SORT_URL":'{“statistics.views.90day”:-1}',
    "NEWEST_APPS_QUERY_URL":'{"status.value":"approved"}',
    "NEWEST_APPS_SORT_URL":'{“created”:-1}',
    "SEARCH_APPS_QUERY_URL":'{"status.value":"approved"}',
    "SEARCH_APPS_FILE_URL":'["name", "customData.summary", "customData.description"]',
    "ANALYTICS_APPS_QUERY_URL":'{"customData.categories":"Analytics"}',
    "COMMUNICATION_APPS_QUERY_URL":'{"customData.categories":"Communication"}',
    "CUSTOMER_SUPPORT_APPS_QUERY_URL":'{"customData.categories":"Customer Support"}',
    "FILE_MANAGEMENT_APPS_QUERY_URL":'{"customData.categories":"File Management"}',
    "PRODUCTIVITY_APPS_QUERY_URL":'{"customData.categories":"Productivity"}',
    "DEVELOPER_TOOLS_APP_QUERY_URL":'{"customData.categories":"Developer Tools"}'
}
 